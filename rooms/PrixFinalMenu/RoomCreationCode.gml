
var rank = 1;
for (var i = 0; i < array_length(Prix.scores); ++i){
	if (Prix.scores[0] < Prix.scores[i])
		rank += 1;
}
rank=clamp(rank, 1, 4);
show_debug_message("Final Prix Rank: " + string(rank));
var filename = object_get_name(Prix.object_index) +".rank";
var writer = file_text_open_write(filename);
file_text_write_real(writer, rank);
file_text_close(writer);