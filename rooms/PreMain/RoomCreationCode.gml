var c_pink = make_color_rgb(255,105,180);
global.thrust = 3;
global.turning = 3;
global.dampening = 3;
global.blends_array = [c_white, c_pink, c_red, c_orange, c_yellow, c_lime, c_aqua, c_fuchsia];
global.blend_index = 0;
global.racer_sprite_array = [Sprite1, Sprite145]
global.racer_sprite_index = 0;


if (file_exists("workshop")){
	var reader = file_text_open_read("workshop");
	global.thrust = file_text_read_real(reader);
	global.turning = file_text_read_real(reader);
	global.dampening = file_text_read_real(reader);
	global.blend_index = file_text_read_real(reader);
	global.racer_sprite_index = file_text_read_real(reader);
	file_text_close(reader);
}

instance_create_depth(0,0,0, DebugController);
instance_create_depth(0,0,0, BackgroundController);
instance_create_depth(0,0,0, MusicController);
instance_create_depth(0,0,0, CameraController);
instance_create_depth(0,0,0, PostProcessingController);

randomize();
room_goto(Main);