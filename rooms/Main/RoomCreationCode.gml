global.mode = "race"

if (instance_exists(Prix)) {
	if (Prix.course_index >= array_length(Prix.courses))
		room_goto(PrixFinalMenu);
	else
		room_goto(PrixPreRaceMenu);	
}

MusicController.shuffle = "MUSIC";