/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

dampening = global.dampening;
thrust = global.thrust;
turning = global.turning;
image_blend = global.blends_array[global.blend_index];

if (abs(x-idle_start_x) > 64 and speed < 0.1) {
	x = 99999;	
}

if x != idle_start_x
	trail_counter += 1;

if (trail_counter == 16){
	trail_x[array_length(trail_x)] = x;	
	trail_y[array_length(trail_y)] = y;
	trail_counter = 0;
}