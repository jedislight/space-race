/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
accepts_input = false;
ignore_try_avoids = false;

// assign color
do{
	var unique = true;
	image_blend = global.blends_array[irandom_range(0, array_length(global.blends_array)-1)]
	with (Racer) {
		if (id != other.id and image_blend == other.image_blend) {
			unique = false;
		}
	}	
} until (unique)
racer_align_appearnce_to_prix()

// AI workshop
var points = global.thrust + global.turning + global.dampening;

var generic_base = floor(points / 3);
var leftover = points - generic_base * 3;
thrust = generic_base;
turning = generic_base;
dampening = generic_base;
if(leftover-- > 0) thrust += 1
if(leftover-- > 0) turning += 1

// setup pathfinding
mp_cell_size = 64;
mp_grid_width = room_width / mp_cell_size;
mp_grid_height = room_height / mp_cell_size;
grid = mp_grid_create(0, 0, room_width / mp_cell_size, room_height / mp_cell_size, mp_cell_size, mp_cell_size);
path = path_add();
temp_path = path_add();
precision = 15 // degrees of error tolerance on pathing goals
path_set_closed(path, false);
trace_color = c_lime;
look_x = 0
look_y = 0;
// AI Personalities
if (image_blend == c_red and thrust < 10 and dampening > 0){
	thrust += 1;
	dampening -= 1
	precision += 5
}
if (image_blend == c_yellow and dampening < 10 and thrust > 1){
	thrust -= 1;
	dampening += 1
	precision -= 2;
}
if (image_blend == c_orange and turning < 9 and thrust > 2 and dampening > 1){
	thrust -= 1;
	dampening -= 1;
	turning += 2;
	precision -= 5
}
if (image_blend == c_lime and turning < 10 and thrust > 1){
	thrust -= 1;
	turning += 1
	precision += 2
}
if (image_blend == c_fuchsia){
	precision += 1;
}
if (image_blend == c_aqua){
	precision -= 1;
}
if (image_blend == c_white){
	precision += 3;
}



// remove if time trial (keep at end or cleanup doesn't work)
if (global.mode == "Time Trial" and instance_number(Racer) > 1) {
	instance_destroy(id);
	exit;
}