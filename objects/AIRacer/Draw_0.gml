/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (DebugController.enabled) {
	draw_set_color(image_blend);

	var camera = view_get_camera(view_current);
	var camera_target = camera_get_view_target(camera);
	if (camera_target == id or not DebugController.mp_grid_drawn){
		DebugController.mp_grid_drawn = true;
		draw_set_alpha(0.25);
		var camera = view_get_camera(view_current);
		var mp_left = camera_get_view_x(camera) div mp_cell_size;
		var mp_right = mp_left + camera_get_view_width(camera) div mp_cell_size;
		var mp_top = camera_get_view_y(camera) div mp_cell_size;
		var mp_bottom = mp_top + camera_get_view_height(camera) div mp_cell_size;
		for(var xx = mp_left-1; xx < mp_right+1; ++xx) for(var yy = mp_top-1; yy < mp_bottom+1; ++yy){
			var cell = mp_grid_get_cell(grid, xx, yy) == 0;
			var left = 0 + mp_cell_size * xx;
			var right = left + mp_cell_size;
			var top = 0 + mp_cell_size * yy;
			var bottom = top + mp_cell_size;
			draw_set_color(c_green);
			if (not cell) draw_set_color(c_red);
			draw_rectangle(left, top, right, bottom, true);
			draw_rectangle(left, top, right, bottom, false);
		}
		draw_set_alpha(1.0);
	}

	if (camera_target == id or view_current == 0) {
		draw_path(path, 0,0, true);

		draw_set_color(trace_color);
		draw_arrow(x,y, look_x, look_y, 5);
	}		

	//cleanup
	draw_set_color(c_white);
}