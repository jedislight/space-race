/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

mp_grid_clear_all(grid);
var in_try_avoid = instance_place(x,y, AI_TRY_AVOID) != noone;
if (not ignore_try_avoids and not in_try_avoid)
	mp_grid_add_instances(grid, AI_TRY_AVOID, true);
mp_grid_add_instances(grid, AI_AVOID, true);
mp_grid_clear_rectangle(grid, x, y, x+1, y+1);// clear self
waypoints = []
waypoints[0] = mp_grid_nearest(grid, Beacon, x,y);
if (instance_exists(waypoints[0])) {
	instance_deactivate_object(waypoints[0]);
}

while (instance_number(Beacon) > 0 and array_length(waypoints) < 3){
	var last = waypoints[array_length(waypoints)-1];
	var next = mp_grid_nearest(grid, Beacon, last.x, last.y);
	waypoints[array_length(waypoints)] = next;
	instance_deactivate_object(next);
}
instance_activate_all();
path_clear_points(path);

for (var i = 0; i < array_length(waypoints); ++i){
	var waypoint = waypoints[i];
	if (not instance_exists(waypoint)) break;
	var last = id;
	if (i > 0) 
		last = waypoints[i-1];
		
	var xx = last.x;
	var yy = last.y;
	if (path_get_number(path)){
		xx = path_get_x(path, 1.0);
		yy = path_get_y(path, 1.0);
	}
	if (mp_grid_path(grid, temp_path, xx, yy, waypoint.x, waypoint.y, true)){
		path_change_point(
			temp_path,
			path_get_number(temp_path)-1,
			waypoint.x,
			waypoint.y,
			path_get_speed(temp_path, 1.0)
		);
		path_append(path, temp_path);	
	} else {
		break;
	}
}
ai_racer_path_cleanup();
if (array_length(waypoints) == 0){
	racer_do_stabalize(1.0);	
	exit;
}
// follow path
var facing_path = false;

var look_ahead_seconds = 1.5;
while(true){
	var look_param = path_get_param(path, max(1,speed)*room_speed*look_ahead_seconds);
	look_x = path_get_x(path, look_param);
	look_y = path_get_y(path, look_param);
	if (collision_line(x, y, look_x, look_y, AI_AVOID, true, true) == noone){
		break;
	}
	look_ahead_seconds *= .9;
	if (look_ahead_seconds < 0.15){
		break;	
	}
}
if (look_x <= 0 and look_y <= 0) {
	look_x = room_width /2;
	look_y = room_height / 2;
}
var look_direction = point_direction(x, y, look_x, look_y);
var look_error = angle_difference(image_angle, look_direction);
facing_path = abs(look_error) < precision * random_range(0.90, 1.1);
var moving_path = false;
move_x = look_x//path_get_point_x(path, 1);
move_y = look_y//path_get_point_y(path, 1);
var move_direction = point_direction(x, y, move_x, move_y)
var move_error = angle_difference(direction, move_direction);
moving_path = abs(move_error) < precision * random_range(0.90, 1.1);
var next_waypoint = waypoints[0];
var distance_to_next_waypoint = distance_to_point(next_waypoint.x, next_waypoint.y);
var moving_to_next_waypoint = place_meeting(x + lengthdir_x(distance_to_next_waypoint, direction), y + lengthdir_y(distance_to_next_waypoint, direction), next_waypoint)
var waypoint_soon = moving_to_next_waypoint and distance_to_next_waypoint < 15*speed;
if (waypoint_soon) {
	show_debug_message("!");
	exit;
}
if (facing_path and (moving_path or speed == 0)){ 
	racer_do_thrust(1.0);
	trace_color = c_lime;
} else if (moving_path) {
	// coast and turn
	var d = -1.0;
	if (look_error < 0) d = 1.0;
	racer_do_turn(d);
} else if (facing_path) {
	// adjust
	racer_do_thrust(1.0);
	
	racer_do_stabalize(1.0)
	
	var d = -1.0;
	if (move_error < 0) d = 1.0;
	racer_do_turn(d);
} else {
	// slow
	racer_do_stabalize(1.0)
	
	// turn
	var d = -1.0;
	if (look_error < 0) d = 1.0;
	racer_do_turn(d);
	
	trace_color = c_red;
}