/// @description Insert description here
// You can write your code in this editor

image_alpha *= 0.99;
var inv_a = 1-image_alpha;
image_xscale = 1 + inv_a;
image_yscale = 1 + inv_a;

if (image_alpha < 0.01) instance_destroy(id)