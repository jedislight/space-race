/// @description Insert description here
// You can write your code in this editor

var racer = other;
var bumper = id;

racer.speed *= -0.5;	
var racer_to_bumper = point_direction(racer.x, racer.y, bumper.x, bumper.y);
with(racer) motion_add(racer_to_bumper, racer.speed*0.5)
racer.x += lengthdir_x(1, racer_to_bumper+180)
racer.y += lengthdir_y(1, racer_to_bumper+180)
show_debug_message("Bumper Collision @ " + string(racer.x) + " " + string(racer.y) + " " + string(racer.image_angle));