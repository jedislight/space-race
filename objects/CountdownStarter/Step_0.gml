/// @description Insert description here
// You can write your code in this editor
counter += 1;
var before = floor(image_index);
image_index = counter / duration * (image_number-1);
var after = floor(image_index);
if (before != after){
	var pitch = 1.0;
	if (image_index == image_number -1){
		pitch = 0.9
	}
	audio_sound_pitch(audio_play_sound(starter_chime, 1, false), pitch);
}
if (image_index >= image_number)
	image_index = image_number - 1

if (counter > duration){
	show_debug_message("Course Start: " + room_get_name(room));
	instance_destroy(id);
}
