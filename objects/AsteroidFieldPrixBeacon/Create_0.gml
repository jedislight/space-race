/// @description Insert description here
// You can write your code in this editor
event_inherited();
size_min = 0;
size_max = 30;
var size = 30 - global.thrust - global.turning - global.dampening;

if (size < size_min or size > size_max){
	image_blend = c_red;		
}

var n = instance_create_depth(0,0,0, AsteroidFieldPrix);
rank = n.rank;
instance_destroy(n);