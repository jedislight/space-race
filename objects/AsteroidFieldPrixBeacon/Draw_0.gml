/// @description Insert description here
// You can write your code in this editor
event_inherited();

var rank_alpha = 1.0;
if (rank == 4) {
	rank_alpha = 0.25;
}
var rank_color = c_dkgray;
if rank == 3
	rank_color = c_orange;
if rank == 2
	rank_color = c_white;
if rank == 1
	rank_color = c_yellow;

draw_sprite_ext(SpriteMedal, 0, x-32, y-32, 1.0, 1.0, 0, rank_color, rank_alpha); 
