/// @description Insert description here
// You can write your code in this editor
mp_grid_drawn = false;

if (ai_time_trials_mode){
	with(Racer) {
	if (object_index == Racer)	
		instance_change(AIRacer, true);
	}
	with(QuickTimeTrialBeacon){
		event_perform(ev_collision, Racer);	
	}
}