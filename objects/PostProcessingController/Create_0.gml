/// @description Insert description here
// You can write your code in this editor
application_surface_draw_enable(false);

bloom_range = 5
bloom_intensity = 1.5;
bloom_threshold = 0.5;

chromatic_aberration_value = 2.5;

vignette_value = 1;

surface_chain = [surface_create(512, 512), surface_create(512, 512)];