/// @description Insert description here
// You can write your code in this editor
//shader_set(PostProcessShader)

function start_chain(){
	if(surface_get_width(surface_chain[0]) != surface_get_width(application_surface) 
		or surface_get_height(surface_chain[0]) != surface_get_height(application_surface)) {
		surface_free(surface_chain[0])
		surface_free(surface_chain[1])		
		surface_chain[0] = surface_create(surface_get_width(application_surface), surface_get_height(application_surface));
		surface_chain[1] = surface_create(surface_get_width(application_surface), surface_get_height(application_surface));
	}
	surface_set_target(surface_chain[0]);
	shader_set(FinalizeAlpha);
	draw_clear(c_black)
	draw_surface(application_surface, 0, 0);
	shader_reset();
	surface_reset_target();
}

function swap_chain(){
	surface_chain = [surface_chain[1], surface_chain[0]];
	surface_set_target(surface_chain[0]);
	draw_clear(c_black)
	draw_surface(surface_chain[1], 0, 0);
	surface_reset_target();
}

function end_chain(){
	var w_height = window_get_height();
	var w_width = window_get_width();
	var draw_dim = min(w_height, w_width);
	var xx = 0;
	var yy = 0;

	if (draw_dim < w_width){
		xx = w_width / 2 - draw_dim / 2;	
	}

	if (draw_dim < w_height){
		yy = w_height / 2 - draw_dim / 2;	
	}

	shader_reset();
	draw_clear(c_black);
	draw_surface_stretched(surface_chain[0], xx, yy, draw_dim, draw_dim )	
}


start_chain();

shader_set(Bloom)
shader_set_uniform_i(shader_get_uniform(Bloom, "range"), bloom_range)
shader_set_uniform_f(shader_get_uniform(Bloom, "intensity"), bloom_intensity);
shader_set_uniform_f(shader_get_uniform(Bloom, "threshold"), bloom_threshold);

swap_chain();

shader_set(ChromaticAberration);
shader_set_uniform_f(shader_get_uniform(ChromaticAberration, "value"), chromatic_aberration_value)
swap_chain();

shader_set(Vignette);
shader_set_uniform_f(shader_get_uniform(Vignette, "value"), vignette_value)
swap_chain();

end_chain();