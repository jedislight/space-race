/// @description Insert description here
// You can write your code in this editor
if (instance_number(AIRacer) > 0 ){
	instance_destroy(id);
	exit;
}

do_thrust = 1 << 0;
do_right  = 1 << 1;
do_left   = 1 << 2;
do_dampen = 1 << 3;

playback = [];
ghost_manager_load()
playback_cursor = 0;
recording = [];
watching = instance_find(Racer, 0);
ghost = noone;
if (array_length(playback) > 0){
	ghost = instance_create_depth(0,0,1, GhostRacer);
	ghost.thrust = playback[playback_cursor++];
	ghost.turning = playback[playback_cursor++];
	ghost.dampening = playback[playback_cursor++];
	ghost.image_blend = playback[playback_cursor++];
	ghost.x = watching.x;
	ghost.y = watching.y;
	ghost.image_angle = watching.image_angle;
}

recording[0] = watching.thrust;
recording[1] = watching.turning;
recording[2] = watching.dampening;
recording[3] = watching.image_blend;
