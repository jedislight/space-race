/// @description Insert description here
// You can write your code in this editor

if (instance_exists(ghost) and playback_cursor < array_length(playback)) {
	var record = playback[playback_cursor++];
	with(ghost){
		if(record & other.do_thrust) racer_do_thrust(1.0);
		if(record & other.do_dampen) racer_do_stabalize(1.0);
		if(record & other.do_left)   racer_do_turn(1.0);
		if(record & other.do_right)  racer_do_turn(-1.0);
	}
	
	if playback_cursor < array_length(playback)
		ghost.x = playback[playback_cursor++];
	if playback_cursor < array_length(playback)
		ghost.y = playback[playback_cursor++];
	if playback_cursor < array_length(playback)
		ghost.image_angle = playback[playback_cursor++];
}