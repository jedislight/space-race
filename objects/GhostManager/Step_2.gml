/// @description Insert description here
// You can write your code in this editor



if (instance_exists(watching)){
	var record = 0;
	if (watching.thrusting) {
		record |= do_thrust;
	}
	if (watching.turning_this_frame < 0){
		record |= do_right;
	}
	if (watching.turning_this_frame > 0){
		record |= do_left;	
	}
	if (watching.stabalizing){
		record |= do_dampen;
	}
	
	array_push(recording, record);
	array_push(recording, watching.x);
	array_push(recording, watching.y);
	array_push(recording, watching.image_angle);
	//show_debug_message("Ghost Record: " + string(record));
}
