/// @description Insert description here
// You can write your code in this editor

weight = (thrust + dampening + turning) * 0.8;
image_xscale = weight / scale_constant + 0.5
image_yscale = image_xscale;

if (not accepts_input) exit;
// zero input
var input_thrust = 0.0;
var input_stabalize = 0.0;
var input_turn = 0.0;

// get input
if keyboard_check(vk_up) input_thrust = 1.0;
if keyboard_check(vk_down) input_stabalize = 1.0;
if keyboard_check(vk_right) input_turn += -1.0;
if keyboard_check(vk_left) input_turn += 1.0;

enum INPUT {
	THRUST = 0,
	STABALIZE = 1,
	RIGHT = 2,
	LEFT = 3,
}
var input_mapping;
input_mapping[0][INPUT.THRUST] = [[-1, vk_up], [0, gp_face1]];
input_mapping[0][INPUT.STABALIZE] = [[-1, vk_down], [0, gp_face2]];
input_mapping[0][INPUT.LEFT] = [[-1, vk_left], [0, gp_padl]];
input_mapping[0][INPUT.RIGHT] = [[-1, vk_right], [0, gp_axislh], [0, gp_padr]];

function input_mapping_resolve(map){
	var results = [];
	for (var row = 0; row < array_length(map); ++row){
		results[row] = 0;
		for (var col = 0; col < array_length(map[row]); ++col){
			var input_cell = map[row][col];
			var device = input_cell[0];
			var key = input_cell[1];
			if (device == -1) {
				results[row] += keyboard_check(key);
			}
			else {
				switch key {
					case gp_axislh:
					case gp_axisrh:
					case gp_axislv:
					case gp_axisrv:
						results[row] += gamepad_axis_value(device, key);
						break;
					default:
						results[row] += gamepad_button_value(device, key);
						break;
				}
			}
		}
	}
	
	return results;
}
var my_input_map = input_mapping[input_mapping_index];
input = input_mapping_resolve(my_input_map);


// input thoughts
/*
 grid input_type == row, binding == column
 cell holding 2 element array [device, key]
 keyboard == -1
 gp == 0+
 [] == no binding
 
 each frame copy grid
 resolve each cell
 sum each row and assign
 
*/

// apply input
racer_do_turn(input[INPUT.LEFT] - input[INPUT.RIGHT]);
racer_do_stabalize(input[INPUT.STABALIZE]);
racer_do_thrust(input[INPUT.THRUST]);