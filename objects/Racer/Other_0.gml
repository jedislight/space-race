/// @description Insert description here
// You can write your code in this editor
if (asset_has_tags(room, "MENU", asset_room)) {
	racer_do_menu_restart();
	exit;
}

var n = instance_create_depth(x,y,depth, DQRacer);
n.image_blend = image_blend;
n.x = clamp(n.x, 0, room_width);
n.y = clamp(n.y, 0, room_height);
instance_destroy(id);