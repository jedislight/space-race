/// @description Insert description here
// You can write your code in this editor
if (!DebugController.enabled) exit;
if (DebugController.draw_inputs and accepts_input){
	input_text = "Input Index: " + string(input_mapping_index);
	for (var i = 0; i < array_length(input); ++i){
		input_text += "\n" + string(input[i]);	
	}
	
	draw_set_valign(fa_bottom);
	draw_text(0, 512, input_text);
	draw_set_valign(fa_top);
}