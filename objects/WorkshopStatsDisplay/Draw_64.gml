/// @description Insert description here
// You can write your code in this editor

function draw_stat_bar(label, value, limit, xx, yy, height, width){
	draw_set_alpha(0.50);
	draw_healthbar(xx,yy,xx+width, yy+height, value/limit*100, c_black, c_green, c_aqua, 0, true, true);
	draw_set_alpha(1.0);
	draw_text_color(xx+1,yy-1, label + " " + string(value), c_black, c_black, c_black, c_black, 1.0);
	draw_text_color(xx-1,yy-1, label + " " + string(value), c_black, c_black, c_black, c_black, 1.0);
	draw_text_color(xx+0,yy-2, label + " " + string(value), c_black, c_black, c_black, c_black, 1.0);
	draw_text_color(xx+0,yy+2, label + " " + string(value), c_black, c_black, c_black, c_black, 1.0);

	draw_text_color(xx,yy-1, label + " " + string(value), c_white, c_white, c_white, c_white, 1.0);
}

draw_stat_bar("Thrust", global.thrust, 10, 16, 16, 16, 160);
draw_stat_bar("Turning", global.turning, 10, 16, 32, 16, 160);
draw_stat_bar("Dampening", global.dampening, 10, 16, 48, 16, 160);
draw_stat_bar("Size", 30 - (global.thrust + global.dampening + global.turning), 28, 16, 64, 16, 160);
