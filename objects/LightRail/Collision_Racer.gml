/// @description Insert description here
// You can write your code in this editor
var racer = other.id

if(place_meeting(x, y, racer)) {
	var offset_x = lengthdir_x(1, image_angle);
	var offset_y = lengthdir_y(1, image_angle);
	racer.x += offset_x;
	racer.y += offset_y;
	//show_debug_message("LightRail ejecting racer: offsets: " + string(offset_x) + " " + string(offset_y));
}

racer.speed *= .75