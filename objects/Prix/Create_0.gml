/// @description Insert description here
// You can write your code in this editor

// setup prix blends
blends = [];
var l = ds_list_create();
for(var i = 0; i < array_length(global.blends_array); ++i){
	if (i != global.blend_index)
		ds_list_add(l, i);	
}
ds_list_shuffle(l);
ds_list_insert(l, 0, global.blend_index);
for(var i=0; i < ds_list_size(l); ++i){
	array_push(blends, l[| i]);
}
ds_list_destroy(l);

bodies = []
repeat (4) 
	array_push(bodies, irandom_range(0, array_length(global.racer_sprite_array)-1));

// setup prix races
courses = []; // inherit - no defaut
course_index = 0;

// setup prix scores
scores = [];
repeat(array_length(blends)) array_push(scores, 0);

// load prix rank
rank = 4;
var filename = object_get_name(object_index) + ".rank";
var reader = file_text_open_read(filename);
if (reader != -1){
	rank = file_text_read_real(reader);
	file_text_close(reader);
}