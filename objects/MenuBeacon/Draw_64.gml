draw_set_alpha(0.25);
draw_sprite(mask_index, 0, x, y);
draw_set_alpha(1.0);
if(sprite_exists(sprite_index))
	draw_self();
draw_text_color(x,y, label, c_white, c_white, c_white, c_white, 0.8);