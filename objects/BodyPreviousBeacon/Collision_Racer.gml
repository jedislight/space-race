event_inherited();
var next = global.racer_sprite_index - 1;
if (next < 0)
	next = array_length(global.racer_sprite_array) - 1;
if (next >= array_length(global.racer_sprite_array))
	next = 0;
	
global.racer_sprite_index= next;

with (other) racer_do_menu_restart()