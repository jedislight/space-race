/// @description Insert description here
// You can write your code in this editor
function make_display_time(t){
	var seconds = 1.0 * t / room_speed;
	var formated = string_format(seconds, 4, 3);
	var colon = string_replace(formated, ".", ":");
	return colon;
}
var display_counter = make_display_time(counter);
var best_counter = make_display_time(best);

draw_set_color(c_white);
draw_text(
	0,
	0,
	display_counter + "\n" + best_counter
);

if (new_record) {
	var color = c_white;
	if (floor(new_record_counter++ / 30) mod 2){
		color = c_black;	
	}
	var display_best = make_display_time(best);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_set_color(color);
	var xx = 256;
	var yy = 256;
	draw_text(xx, yy,
		"New Record!\n" + display_best);
	draw_set_valign(fa_top);
	draw_set_halign(fa_left);
}