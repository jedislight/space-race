/// @description Insert description here
// You can write your code in this editor
if (CourseCompletionWatcher.transition > 0 and instance_number(Beacon) == 0){
	if (counter < best) {
		best = counter
		var best_file = file_text_open_write(best_filename);
		if (best_file != -1) {
			file_text_write_real(best_file, best);
			file_text_close(best_file);
			ghost_manager_save();
			new_record = true;
		}
	}
} else if (CourseCompletionWatcher.transition == -1 and instance_number(CountdownStarter) == 0){
	counter += 1;
}