/// @description Insert description here
// You can write your code in this 
if (instance_number(AIRacer) > 0 ){
	instance_destroy(id);
	exit;
}
counter = 0;
best = 999999;
best_filename = room_get_name(room)+".best"
new_record = false;
new_record_counter = 0;
if (file_exists(best_filename)){
	var best_file = file_text_open_read(best_filename);
	if (best_file != -1) {
		var file_best = file_text_read_real(best_file);
		best = file_best;	
		file_text_close(best_file);
	}
}