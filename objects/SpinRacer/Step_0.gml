/// @description Insert description here
// You can write your code in this editor
event_inherited()
var da = angle_difference(image_angle, 90);
if (counter > room_speed+10 and sign(da) != sign(prev_da) and abs(da) < 90)
{
	counter = -1;
	image_angle = 90;
}
prev_da = da;

counter += 1;

if (counter > room_speed) {
	racer_do_turn(-1.0);	
}
if ((counter mod (floor(room_speed / 5))) == 0){
	var n = instance_create_depth(x,y,1, AngularPulse);	
	n.image_angle = image_angle
}