/// @description Insert description here
// You can write your code in this editor
if (audio_exists(next)){
	if(audio_exists(playing) and skip){
		audio_stop_sound(playing);
	}
	
	if (not audio_exists(playing)){
		skip =  false;
		playing = audio_play_sound(next, 1, loop);
		next = noone;
	}
} else if ((is_string(shuffle) or is_array(shuffle) and array_length(shuffle) > 0) and not audio_exists(playing)){
	var music_choices = tag_get_asset_ids(shuffle, asset_sound);
	var num_choices = array_length(music_choices);
	show_debug_message("Shuffle music from " + string(num_choices) + " options");
	var choice_index = irandom_range(0, array_length(music_choices)-1);
	var choice = music_choices[choice_index];
	playing = audio_play_sound(choice, 1, false);
}