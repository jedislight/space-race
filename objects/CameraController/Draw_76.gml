/// @description Insert description here
// You can write your code in this editor

if(global.mode == "Time Trial"){
	for(var i = 0; i < instance_number(Racer); ++i){
		var racer = instance_find(Racer, i);
		if (racer.object_index == GhostRacer) continue;
		target_index = i;
		break;
	}
}

var camera = view_get_camera(0);
camera_set_view_target(camera, instance_find(Racer, target_index));

function set_quad_split(view_index, target){
	var camera = view_get_camera(view_index);
	camera_set_view_size(camera, 512, 512);
	camera_set_view_target(camera, target);
	camera_set_view_border(camera, 216, 216)
	var view_x = 0;
	var view_y = 0;
	if (view_index == 2 or view_index == 4) view_x = 256;
	if (view_index == 3 or view_index == 4) view_y = 256;
	view_set_xport(view_index, view_x)
	view_set_yport(view_index, view_y)
	view_set_wport(view_index, 256);
	view_set_hport(view_index, 256);
}

set_quad_split(1, instance_find(Racer,0));
set_quad_split(2, instance_find(Racer,1));
set_quad_split(3, instance_find(Racer,2));
set_quad_split(4, instance_find(Racer,3));

if( asset_has_tags(room, "MENU", asset_room)){
	view_set_hport(0, 512);	
	view_set_wport(0, 512);
}