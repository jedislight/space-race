/// @description Insert description here
// You can write your code in this editor
var v_width = view_get_wport(view_current);
var v_height = view_get_hport(view_current);
if (v_width != 512 or v_height != 512) {
	
	var camera = view_get_camera(view_current);
	var cx = camera_get_view_x(camera);
	var cy = camera_get_view_y(camera);
	var cw = camera_get_view_width(camera);
	var ch = camera_get_view_height(camera);
	draw_set_color(c_white);
	for(var i = 0; i < 5; ++i){
		draw_rectangle(cx+i, cy+i, cx+cw-i, cy+ch-i, true)	
	}
}