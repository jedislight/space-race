event_inherited();
var writer = file_text_open_write("workshop");
file_text_write_real(writer, global.thrust);
file_text_write_real(writer, global.turning);
file_text_write_real(writer, global.dampening);
file_text_write_real(writer, global.blend_index);
file_text_write_real(writer, global.racer_sprite_index);
file_text_close(writer);
room_goto(Main)