event_inherited();
var next = global.blend_index + 1;
if (next < 0)
	next = array_length(global.blends_array) - 1;
if (next >= array_length(global.blends_array))
	next = 0;
	
global.blend_index = next;

with (other) racer_do_menu_restart()