/// @description Insert description here
// You can write your code in this editor

var num_beacon = instance_number(Beacon)
var num_racer = instance_number(Racer)
var num_ai_racer = instance_number(AIRacer)
if (transition < 0 and (
	   num_beacon == 0 
	or num_racer + num_ai_racer == 0)
	) {
	transition = 100;
	show_debug_message("Course Complete: " + room_get_name(room));
	var blend_counts = ds_map_create();
	with(HitBeacon) {
		var count = ds_map_find_value(blend_counts, image_blend);
		if (is_undefined(count)) count = 0;
		blend_counts[? image_blend] = count + 1;
		hit_count = 0;
	}
	
	var winning_count = 0;
	for (var blend = ds_map_find_first(blend_counts); not is_undefined(blend); blend = ds_map_find_next(blend_counts, blend)){
		var count = blend_counts[? blend];
		winning_count = max(winning_count, count);
	}
	
	for (var blend = ds_map_find_first(blend_counts); not is_undefined(blend); blend = ds_map_find_next(blend_counts, blend)){
		var count = blend_counts[? blend];
		if winning_count != count
			with(HitBeacon) if image_blend == blend
				instance_destroy(id);
	}
	
	ds_map_destroy(blend_counts);
	
	with (Racer) instance_destroy(id);
	with (Prix) course_index += 1;
}

if (transition > 0) {
	var t = 1;
	if (instance_number(Timer) > 0 and Timer.new_record){
		t = 0.3;
	}
	transition -= t;
	
	if (transition = 0)
		room_goto(Main);
	else with(HitBeacon) {
		depth = -100;
		image_xscale *= 1.05	
		image_yscale *= 1.05
	}
}