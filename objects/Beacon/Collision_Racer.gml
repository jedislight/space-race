/// @description Insert description here
// You can write your code in this editor
if (other.object_index == GhostRacer) exit;
if(instance_number(Beacon) == 1 and is_circut) {
	var oldest_hit_beacon = noone;
	var oldest_hit_frame = frame;
	with(HitBeacon) {
		if (hit_count < 3 and hit_frame < oldest_hit_frame){
			oldest_hit_frame = hit_frame;
			oldest_hit_beacon = id;
		}
	}
	
	with(oldest_hit_beacon) {
		instance_change(Beacon, false);
		image_blend = c_white;
		show_debug_message("Reset hit beacon " + string(x) + " " + string(y))
	}
}

show_debug_message("Pre-Prix Handling");
var prix_index = 0;
if (other.object_index == AIRacer){
	prix_index = other.index;	
}
show_debug_message("Prix Racer Index:" + string(prix_index));	
with(Prix){
	scores[prix_index] += 1;
	show_debug_message("Prix Score For Racer " + string(prix_index));	
}

instance_change(hit_object_index, false);
image_blend = other.image_blend
hit_frame = frame
hit_count += 1