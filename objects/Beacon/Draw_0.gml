/// @description Insert description here
// You can write your code in this editor
draw_self();

var camera = view_get_camera(view_current);
var left = camera_get_view_x(camera);
var top = camera_get_view_y(camera);
var right = camera_get_view_x(camera) + camera_get_view_width(camera);
var bottom = camera_get_view_y(camera) + camera_get_view_height(camera);

var in_camera =( x+sprite_width > left and x-sprite_width < right and y+sprite_height > top and y-sprite_height < bottom);

if ( not in_camera){
	var center_xx = camera_get_view_width(camera) / 2 + camera_get_view_x(camera);
	var center_yy = camera_get_view_height(camera) / 2 + camera_get_view_y(camera);
	var dir_center_to_me = point_direction(center_xx, center_yy, x,y);

	var clamp_x = clamp(x, left, right);
	var clamp_y = clamp(y, top, bottom);
	var d = abs(clamp_x - x) + abs(clamp_y - y)
	var scale = 1.0 - (d/2000);
	scale = clamp(scale, 0.0, 1.0);
	draw_sprite_ext(Sprite16, 0 , clamp_x, clamp_y, scale, scale, dir_center_to_me, c_white, 1.0); 
}