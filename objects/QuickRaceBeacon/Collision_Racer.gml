/// @description Insert description here
// You can write your code in this editor
event_inherited();
var rooms = [];
global.mode = "Race"

for(var next = room_next(room_first); room_exists(next); next = room_next(next))
	if (
		    string_starts_with(room_get_name(next), "Course_")
		and asset_has_tags(next, "RACE", asset_room)
	)
		rooms[array_length(rooms)] = next;

var random_room_index = irandom_range(0, array_length(rooms)-1);
room_goto(rooms[random_room_index]);