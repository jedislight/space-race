{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 35,
  "bbox_right": 58,
  "bbox_top": 10,
  "bbox_bottom": 53,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a21776b1-8a15-46d7-850a-49ec9b97afaa","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a21776b1-8a15-46d7-850a-49ec9b97afaa","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":{"name":"094573bf-1abd-4212-9f80-52acac94aac6","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"a21776b1-8a15-46d7-850a-49ec9b97afaa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0cb85990-f844-4d03-901e-929ffa17eea3","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0cb85990-f844-4d03-901e-929ffa17eea3","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":{"name":"094573bf-1abd-4212-9f80-52acac94aac6","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"0cb85990-f844-4d03-901e-929ffa17eea3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04eb08d9-8783-40e8-a86a-cf315aaee6ff","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04eb08d9-8783-40e8-a86a-cf315aaee6ff","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":{"name":"094573bf-1abd-4212-9f80-52acac94aac6","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"04eb08d9-8783-40e8-a86a-cf315aaee6ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9ce9e4d7-e23a-4410-bcd1-5a3f2286452c","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9ce9e4d7-e23a-4410-bcd1-5a3f2286452c","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":{"name":"094573bf-1abd-4212-9f80-52acac94aac6","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"9ce9e4d7-e23a-4410-bcd1-5a3f2286452c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c13efb1c-1d39-4970-821e-9db172897d7b","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c13efb1c-1d39-4970-821e-9db172897d7b","path":"sprites/Sprite145/Sprite145.yy",},"LayerId":{"name":"094573bf-1abd-4212-9f80-52acac94aac6","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","name":"c13efb1c-1d39-4970-821e-9db172897d7b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"037ba4f1-e931-45e8-be2e-d5f493e08d45","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a21776b1-8a15-46d7-850a-49ec9b97afaa","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6653ee00-462a-499c-acb2-879487b1b81a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0cb85990-f844-4d03-901e-929ffa17eea3","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8268db3-ee5c-4390-955f-46cec0d1fb84","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04eb08d9-8783-40e8-a86a-cf315aaee6ff","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6e3d855-9b8f-4046-8054-f5d566759649","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9ce9e4d7-e23a-4410-bcd1-5a3f2286452c","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8615aee-6358-4ab4-8475-a57226794bb0","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c13efb1c-1d39-4970-821e-9db172897d7b","path":"sprites/Sprite145/Sprite145.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 47,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite145","path":"sprites/Sprite145/Sprite145.yy",},
    "resourceVersion": "1.3",
    "name": "Sprite145",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"094573bf-1abd-4212-9f80-52acac94aac6","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Sprites",
    "path": "folders/Racer/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sprite145",
  "tags": [],
  "resourceType": "GMSprite",
}