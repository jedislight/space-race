{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 511,
  "bbox_top": 0,
  "bbox_bottom": 511,
  "HTile": true,
  "VTile": true,
  "For3D": false,
  "width": 512,
  "height": 512,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4c31fc38-8ee4-407f-8fbe-6a8e964c0daf","path":"sprites/background6/background6.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4c31fc38-8ee4-407f-8fbe-6a8e964c0daf","path":"sprites/background6/background6.yy",},"LayerId":{"name":"acf61e5d-65ef-4186-ad5d-7faab2878674","path":"sprites/background6/background6.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"background6","path":"sprites/background6/background6.yy",},"resourceVersion":"1.0","name":"4c31fc38-8ee4-407f-8fbe-6a8e964c0daf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"background6","path":"sprites/background6/background6.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f5bd375b-a6fd-4048-858c-a5428d822747","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4c31fc38-8ee4-407f-8fbe-6a8e964c0daf","path":"sprites/background6/background6.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"background6","path":"sprites/background6/background6.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"acf61e5d-65ef-4186-ad5d-7faab2878674","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Backgrounds",
    "path": "folders/Backgrounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "background6",
  "tags": [],
  "resourceType": "GMSprite",
}