// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function ai_racer_path_cleanup(){
	//remove extra path points before centering
	
	for(var pp = 1; pp < path_get_number(path)-1 and path_get_length(path) > 2; ++pp){
		var pp_dir = path_direction(path, pp);
		var next_dir = path_direction(path, pp+1);
		var prev_dir = path_direction(path, pp-1);

		if (pp_dir == next_dir and next_dir == prev_dir){
			path_delete_point(path, pp);
			--pp;
		}
	}
	
	var list = ds_list_create();
	var clean_path = path_duplicate(path);
	for(var pp = 1; pp < path_get_number(path); ++pp){
		var point_dir = path_direction(path, pp);
		var left_dir = point_dir + 90;
		var right_dir = point_dir - 90;
		
		var p_x = path_get_point_x(path, pp);
		var p_y = path_get_point_y(path, pp);
		
		var ray_length = 256;
		
		var r_x = p_x + lengthdir_x(ray_length, right_dir);
		var r_y = p_y + lengthdir_y(ray_length, right_dir);
		
		var l_x = p_x + lengthdir_x(ray_length, left_dir);
		var l_y = p_y + lengthdir_y(ray_length, left_dir);
		
		ds_list_clear(list);
		var r_hit = collision_line_list(p_x, p_y, r_x, r_y, AI_AVOID, true, true, list, true);
		if (r_hit == 0) continue;
		var r_dist = 0;
		with (list[|0]) r_dist = distance_to_point(p_x, p_y);
		
		ds_list_clear(list);
		var l_hit = collision_line_list(p_x, p_y, l_x, l_y, AI_AVOID, true, true, list, true);
		if (l_hit == 0) continue;
		var l_dist = 0;
		with (list[|0]) l_dist = distance_to_point(p_x, p_y);
		
		var t_dist = l_dist + r_dist;
		var middle_distance = t_dist / 2;
		var correction_dist = middle_distance - l_dist;
		
		var middle_x = p_x + lengthdir_x(correction_dist, right_dir);
		var middle_y = p_y + lengthdir_y(correction_dist, right_dir);
		
		path_change_point(clean_path, pp, middle_x, middle_y, path_get_speed(path, pp));
	}
	
	path_set_kind(clean_path, true);
	
	ds_list_destroy(list);
	path_delete(path);
	path = clean_path;
}