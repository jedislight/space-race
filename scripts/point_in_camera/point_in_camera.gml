// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function point_in_camera(camera, xx, yy){
	var left = camera_get_view_x(camera);
	var right = left + camera_get_view_width(camera);
	var top = camera_get_view_y(camera);
	var bottom = top + camera_get_view_height(camera);
	
	return xx > left and xx < right and yy > top and yy < bottom;
}