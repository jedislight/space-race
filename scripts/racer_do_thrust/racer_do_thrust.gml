// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_do_thrust(amount){
with (RACER_LOCK) exit;
amount = clamp(amount, 0.0, 1.0);
motion_add(image_angle, (amount * thrust) / weight * thrust_constant);
thrusting = amount > 0.0;
}