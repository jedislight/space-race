// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function path_direction(path, index){
	var path_size = path_get_number(path);
	if (index == path_size -1){
		index -= 1;	
	}
	if (index == 0) {
		index = 1;	
	}
	var start_x = path_get_point_x(path, index);
	var start_y = path_get_point_y(path, index);
	var next_x = path_get_point_x(path, index +1);
	var next_y = path_get_point_y(path, index +1);
	var prev_x = path_get_point_x(path, index -1);
	var prev_y = path_get_point_y(path, index -1);
	
	var sn = point_direction(start_x, start_y, next_x, next_y);
	var ps = point_direction(prev_x, prev_y, start_x, start_y);
	return sn + angle_difference(sn, ps) / 2;
}