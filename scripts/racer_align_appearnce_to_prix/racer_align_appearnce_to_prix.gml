// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_align_appearnce_to_prix(){
	if(instance_number(Prix) > 0) {
		var prix = instance_find(Prix, 0);
		image_blend = global.blends_array[prix.blends[index]];	
		sprite_index = global.racer_sprite_array[prix.bodies[index]];
	}
	
	if (index == 0){
		image_blend = global.blends_array[global.blend_index];	
		sprite_index = global.racer_sprite_array[global.racer_sprite_index];
	}
}