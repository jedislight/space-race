// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_do_stabalize(amount){
with (RACER_LOCK) exit;
amount = clamp(amount, 0.0, 1.0);
var drag = (amount * dampening) / weight * dampening_constant;
speed = speed * (100-drag) / 100
stabalizing = amount > 0;
}