// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_do_turn(amount){
	with (RACER_LOCK) exit;
	amount = clamp(amount, -1.0, 1.0);
	image_angle += turn_constant * (turning * amount) / weight;
	turning_this_frame += amount;
}