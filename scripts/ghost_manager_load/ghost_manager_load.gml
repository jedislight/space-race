// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ghost_manager_load(){
	with(GhostManager){
		var filename = room_get_name(room)+".ghost"
		var reader = file_text_open_read(filename);
		if (reader == -1) return;
		while(not file_text_eof(reader))
			array_push(playback, file_text_read_real(reader));
		file_text_close(reader);
	}
}