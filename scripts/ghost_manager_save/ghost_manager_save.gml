// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ghost_manager_save(){
	with(GhostManager){
		var filename = room_get_name(room)+".ghost"
		var writer = file_text_open_write(filename);
		for(var i = 0; i < array_length(recording); ++i)
			file_text_write_real(writer, recording[i]);
		file_text_close(writer);
	}
}