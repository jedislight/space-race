// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function mp_grid_nearest(grid, object, x_start, y_start){
	var path = path_add();
	var result = noone;
	var distance = 99999;
	
	for (var i = 0; i < instance_number(object); ++i) {
		var c = instance_find(object, i);
		if (mp_grid_path(grid, path, x_start, y_start, c.x, c.y, true)) {
			var c_distance = path_get_number(path);
			if (c_distance < distance) {
				distance = c_distance;
				result = c;
			}
		}
	}

	path_delete(path);
	
	if (not instance_exists(result)) {
		result = instance_nearest(x_start, y_start, object);	
	}
	return result;
}