// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function path_get_param(path, distance){
	var t = path_get_length(path);
	return clamp(distance/t, 0.0, 1.0);
}