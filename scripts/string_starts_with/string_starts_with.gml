// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function string_starts_with(s, value){
	if (string_length(value) > string_length(s)) return false;
	
	var start = string_copy(s, 1, string_length(value));
	return start == value;
}