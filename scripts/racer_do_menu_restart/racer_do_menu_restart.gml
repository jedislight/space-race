// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_do_menu_restart(){
	if( object_is_ancestor(object_index, MenuBeacon) ) {
		with (Racer) racer_do_menu_restart();
		return;
	}
	x = room_width / 2;
	y = room_height / 2;
	speed = 0;
	direction = 0;
	if (room != Workshop)
		image_angle = 0;
	dampening = global.dampening;
	thrust = global.thrust;
	turning = global.turning;
	image_blend = global.blends_array[global.blend_index];
	sprite_index = global.racer_sprite_array[global.racer_sprite_index];
}