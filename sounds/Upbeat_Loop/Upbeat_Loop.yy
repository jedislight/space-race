{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Upbeat_Loop.ogg",
  "duration": 21.3383331,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "Upbeat_Loop",
  "tags": [
    "MUSIC",
  ],
  "resourceType": "GMSound",
}