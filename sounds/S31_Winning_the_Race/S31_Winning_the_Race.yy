{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "S31_Winning_the_Race.ogg",
  "duration": 139.039474,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "S31_Winning_the_Race",
  "tags": [
    "MUSIC",
  ],
  "resourceType": "GMSound",
}