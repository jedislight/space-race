{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Cyberpunk_Moonlight_Sonata_v2.mp3",
  "duration": 116.7462,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "Cyberpunk_Moonlight_Sonata_v2",
  "tags": [
    "MUSIC",
  ],
  "resourceType": "GMSound",
}