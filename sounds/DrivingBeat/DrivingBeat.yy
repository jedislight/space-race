{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "DrivingBeat.mp3",
  "duration": 64.03108,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "DrivingBeat",
  "tags": [
    "MUSIC",
  ],
  "resourceType": "GMSound",
}