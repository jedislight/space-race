//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

float color_distance(vec3 a, vec3 b) {
	float sum = abs(a.r-b.r) + abs(a.g - b.g) + abs(a.b - b.b);
	return sum / 3.0; // 0.0 -> 1.0
}

void main()
{
    vec4 myColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	float luminence = 0.0;
	float luminencei = 0.0;
	const float c = 1.0 / 512.0;
	const float passes = 1.0;
	for (float i = 1.0; i <= passes; ++i) {
		vec2 offset = vec2(-1.0* i * c, -1.0 * i * c);
		vec4 nextColor = texture2D( gm_BaseTexture, v_vTexcoord	+ offset);
		vec4 prevColor = texture2D( gm_BaseTexture, v_vTexcoord	- offset);
		float d = color_distance(nextColor.rgb, myColor.rgb);
		float di = color_distance(prevColor.rgb, myColor.rgb);
		luminence = luminence + d;
		luminencei = luminencei + di;
	}
	luminence = luminence / passes; // 0.0 -> 1.0;
	luminencei = luminencei / passes;
	luminencei = min(luminencei, luminence);
	luminence = abs(luminence - luminencei);
	vec3 lum = vec3(luminence, luminence, luminence);
	gl_FragColor.rgb = lum;
	gl_FragColor.rgb = myColor.rgb + lum;
	gl_FragColor.a = 1.0;
	
}
