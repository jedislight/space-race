//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float value;

const float pixelSize = 1.0/512.0;


void main()
{

	float distanceFromCenter = distance(vec2(0.5, 0.5), v_vTexcoord);
	float centering = clamp(1.0 - distanceFromCenter * distanceFromCenter * value, 0.0, 1.0);
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	gl_FragColor.rgb *= centering;
}
