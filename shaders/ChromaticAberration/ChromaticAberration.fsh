//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float value;

const float pixelSize = 1.0/512.0;


void main()
{

	float distanceFromCenter = distance(vec2(0.5, 0.5), v_vTexcoord);
	float centering = distanceFromCenter * distanceFromCenter;
	vec2 offset = vec2(pixelSize * value * centering, 0.0);
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	gl_FragColor.g = v_vColour.b * texture2D( gm_BaseTexture, v_vTexcoord + offset ).g;
	gl_FragColor.b = v_vColour.b * texture2D( gm_BaseTexture, v_vTexcoord - offset ).b;	
	//gl_FragColor.r = offset.x;
}
