//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform int range;
uniform float intensity;
uniform float threshold;
void main()
{
    vec4 myColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 finalColor = myColor;
	for(int xx = -range; xx <= range; ++xx) for(int yy = -range; yy <= range; ++yy){
		if (abs(float(xx)) + abs(float(yy)) > float(range)) continue;
		vec4 sample = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord + vec2(float(xx), float(yy)) / 512.0 );	
		float sample_intensity = float(range)-((abs(float(xx)) + abs(float(yy))) /2.0);
		sample_intensity = intensity * 0.01;
		if (sample.r > threshold || sample.g > threshold || sample.b > threshold) {
			finalColor.r += sample.r * sample_intensity * intensity;
			finalColor.g += sample.g * sample_intensity * intensity;
			finalColor.b += sample.b * sample_intensity * intensity;
		}
	}
	gl_FragColor = finalColor;
}
