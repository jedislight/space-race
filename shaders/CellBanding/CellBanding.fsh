//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform int banding;

float band(float value, int bands){
	return float(int(value * float(bands))) / float(bands);
	
}
void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	gl_FragColor.r = band(gl_FragColor.r, banding);
	gl_FragColor.g = band(gl_FragColor.g, banding);
	gl_FragColor.b = band(gl_FragColor.b, banding);
	
}
